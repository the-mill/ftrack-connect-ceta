# :coding: utf-8
# :copyright: Copyright (c) 2014 ftrack
# :license: See README.rst

import logging
from datetime import timedelta

import ftrack

from .transformer import Transformer, TransformerError

#: Key under which to store Ceta meta information in ftrack metadata.
CETA_META_NAME = 'cetaId'


class Planning(object):
    '''Sync bookings from Ceta resources to ftrack Planning.'''

    def __init__(self, cetaAPI, transformer=None, acceptedCategories=None):
        '''Initiate with *cetaAPI* and *transformer*.

        *acceptedCategories* can be used to restrict what categories should be
        used for syncing. Any resource with a category that is not in
        *acceptedCategories* will subsequently be skipped during sync.

        '''
        self.logger = logging.getLogger(
            __name__ + '.' + self.__class__.__name__
        )
        self.logger.info(
            u'Initialising planning sync with Ceta API {0}'.format(
                cetaAPI
            )
        )
        self.cetaAPI = cetaAPI

        # Skip all categories except those in acceptedCategories.
        self.acceptedCategories = acceptedCategories

        if not transformer:
            transformer = Transformer()
        self.transformer = transformer

    def sync(self):
        '''Sync all collected data to ftrack server.'''
        try:
            for projectData in self.cetaAPI.getProjects():

                try:
                    project = self.transformer.toFtrackProject(projectData)
                except TransformerError as error:
                    self.logger.warning(error)
                    continue

                bookings = self._processResources(
                    projectData.get('resources', [])
                )

                self._syncBookings(project, bookings)

        except:
            self.logger.exception('Exception during sync, aborting.')
            raise

    def _processResources(self, rawResources):
        '''Return bookings from *rawResources*.'''
        if not rawResources:
            return []

        bookings = []
        for resource in rawResources:
            if (
                self.acceptedCategories
                and resource['category'] not in self.acceptedCategories
            ):
                continue

            try:
                user = self.transformer.toFtrackUser(resource)
            except TransformerError as error:
                self.logger.warning(error)
                continue

            for schedule in resource['schedule']:
                startdate = self.transformer.toDatetime(schedule['start_time'])
                enddate = self.transformer.toDatetime(schedule['end_time'])

                # Add one extra day to end date to make single-day jobs show up
                # correct in ftrack Planning.
                enddate = enddate + timedelta(days=1)

                status = schedule['status']
                cetaId = schedule['id']

                bookings.append({
                    'user': user,
                    'startdate': startdate,
                    'description': status,
                    'cetaId': cetaId,
                    'enddate': enddate
                })

        self.logger.debug(
            u'Ceta parser found {0} bookings'.format(len(bookings))
        )

        return bookings

    def _syncBookings(self, project, bookings):
        '''Add or update *bookings* on the *project*.'''
        self.logger.info(
            u'Syncing {0} bookings for project {1}'.format(
                len(bookings), project.getName()
            )
        )

        # Build dictionary to see if we should update or create new booking
        cetaIdsLookup = {}
        for booking in project.getBookings():
            if booking.getMeta(CETA_META_NAME):
                cetaIdsLookup[booking.getMeta(CETA_META_NAME)] = booking

        for bookingData in bookings:
            # Determine if this is an existing booking (update or not)
            cetaId = str(bookingData.get('cetaId'))
            existingBooking = cetaIdsLookup.get(cetaId)

            if existingBooking:
                # Delete the key from lookup to keep track if the booking
                # has been removed from Ceta.
                del cetaIdsLookup[cetaId]
                self._updateBooking(existingBooking, bookingData)

            else:
                self._addBooking(project, bookingData)

        # Any bookings still present in the lookup has been removed from Ceta.
        # Therefore remove them from ftrack.
        for booking in cetaIdsLookup.values():
            self.logger.debug(u'Deleting booking {0}'.format(booking))
            booking.delete()

    def _updateBooking(self, existingBooking, newData):
        '''Update *existingBooking* from *newData*.'''
        self.logger.debug(
            u'Updating booking: {0} with {1}'.format(existingBooking, newData)
        )

        syncKeys = ('startdate', 'enddate', 'description')
        copyThese = dict((key, newData[key]) for key in syncKeys)
        existingBooking.set(copyThese)

    def _addBooking(self, project, bookingData):
        '''Add new booking to *project* from *bookingData*.'''
        self.logger.debug(u'Adding booking: {0}'.format(bookingData))

        newBooking = bookingData['user'].createBooking(
            bookingData['description'],
            bookingData['startdate'],
            bookingData['enddate'],
            project
        )

        # Read only will be respected from ftrack web interface. cetaId is used
        # to match the booking when updating.
        newBooking.setMeta('readOnly', True)
        newBooking.setMeta(CETA_META_NAME, bookingData['cetaId'])


class Project(object):
    '''Sync project related data to ftrack from ceta.'''

    def __init__(self, cetaAPI, defaultScheme, transformer=None):
        '''Initiate with *cetaAPI* to Ceta server and project *defaultScheme*'''
        self.logger = logging.getLogger(
            __name__ + '.' + self.__class__.__name__
        )
        self.logger.info(
            u'Initialising project sync with Ceta API: {0}'.format(
                cetaAPI
            )
        )
        self.cetaAPI = cetaAPI
        self.defaultScheme = defaultScheme

        if not transformer:
            transformer = Transformer()
        self.transformer = transformer

        # Create lookup dictionary of currently synced projects.
        self._syncedProjects = {}
        for project in ftrack.getProjects():
            cetaId = project.getMeta(CETA_META_NAME)
            if cetaId is not None:
                self._syncedProjects[cetaId] = project

    def sync(self):
        '''Sync project data from Ceta to ftrack.'''
        self.logger.debug(u'Syncing projects.')

        for projectData in self.cetaAPI.getProjects():
            self._syncProject(projectData)

    def _syncProject(self, projectData):
        '''Sync *data* to ftrack and create new projects if no match is found.

        New projects will be created with *defaultScheme* as project scheme.
        Existing projects are synced by cetaId in metaData.

        '''
        cetaId = projectData['id']
        project = self._syncedProjects.get(str(cetaId))

        if project is None:
            code = projectData['guid']
            fullname = projectData['title']
            self.logger.debug(u'Creating new project: {0}'.format(code))

            project = ftrack.createProject(fullname, code, self.defaultScheme)
            project.setMeta(CETA_META_NAME, cetaId)

        self.logger.debug(
            u'Syncing project data to: {0}'.format(project.getName())
        )

        project.set({
            'startdate': self.transformer.toDatetime(projectData['start_date']),
            'enddate': self.transformer.toDatetime(projectData['end_date'])
        })
