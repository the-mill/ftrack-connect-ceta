# :coding: utf-8
# :copyright: Copyright (c) 2014 ftrack
# :license: See README.rst

from datetime import datetime

import ftrack


class TransformerError(Exception):
    '''Raise when there is an error in the transformer.'''

    defaultMessage = 'Unspecified transformer error.'

    def __init__(self, message=None):
        '''Initialise exception with *message*.

        If *message* is None, the class 'defaultMessage' will be used.

        '''
        if message is None:
            message = self.defaultMessage

        super(TransformerError, self).__init__(message)


class Transformer(object):
    '''Class to translate raw ceta data to a format suitable for ftrack.'''

    def __init__(self):
        self.userCache = None
        self.projectCache = None

    @property
    def users(self):
        '''Dictionary of :py:class:`ftrack.User` with username as key.'''
        if self.userCache is None:
            self.userCache = dict(
                (user.get('username'), user)
                for user in ftrack.getUsers()
            )

        return self.userCache

    @property
    def projects(self):
        '''Dictionary of :py:class:`ftrack.Project` with project code as key.'''
        if self.projectCache is None:
            self.projectCache = dict(
                (project.getName(), project)
                for project in ftrack.getProjects()
            )

        return self.projectCache

    def toDatetime(self, cetaDate):
        '''Return :py:class:`datetime:datetime` from *cetaDate*.

        *cetaDate* is assumed to be formatted like:
        {
            "entity_type": "datetime",
            "timezone": "utc",
            "value": "2013-10-10"
        }

        '''
        parsedDateTime = datetime.strptime(cetaDate['value'], '%Y-%m-%d')

        if not isinstance(parsedDateTime, datetime):
            raise TransformerError(
                'Could not convert {0} to datetime.datetime'.format(
                    cetaDate
                )
            )

        return parsedDateTime

    def toFtrackUser(self, raw):
        '''Return :py:class:`ftrack.User` from ceta *raw* data.'''
        userName = raw.get('user_name')
        if userName is None:
            raise TransformerError(
                'Cannot map user with no user_name: {0}.'.format(raw)
            )

        if userName not in self.users:
            raise TransformerError(
                '{0} is not an existing ftrack username.'.format(userName)
            )

        return self.users[userName]

    def toFtrackProject(self, raw):
        '''Return :py:class:`ftrack.Project` from ceta *raw* data.'''
        guid = raw.get('guid')

        if raw.get('entity_type') != 'project':
            raise TransformerError('entity type is assumed to be project')

        if not guid:
            raise TransformerError('raw guid must exist')

        if guid not in self.projects:
            raise TransformerError(
                '{0} is not an existing ftrack project code.'.format(guid)
            )

        return self.projects[guid]
