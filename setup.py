# :coding: utf-8
# :copyright: Copyright (c) 2014 ftrack
# :license: See README.rst

import os

from setuptools import setup, find_packages

readme_path = os.path.join(os.path.dirname(__file__), 'README.rst')
packages_path = os.path.join(os.path.dirname(__file__), 'source')

setup(
    name='ftrack-connect-ceta',
    version='0.1.0',
    description='Pull data from Ceta server and synchronize with ftrack.',
    long_description=open(readme_path).read(),
    keywords='ftrack, connect, ceta',
    url='https://bitbucket.org/ftrack/ftrack-connect-ceta',
    author='ftrack',
    author_email='support@ftrack.com',
    packages=find_packages(packages_path),
    package_dir={
        '': 'source'
    },
    zip_safe=False
)
